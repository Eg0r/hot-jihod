package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.filters.GlowFilter;
	import flash.net.SharedObject;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Score extends Sprite
	{
		public function Score(num: int)
		{
			var hs: DisplayObject = new Resource.MHS();
			hs.width *= EEConstants.Koef;
			hs.height *= EEConstants.Koef;
			hs.y = EEConstants.GeneralHeight * 0.61; //435 * EEConstants.Koef;
			this.addChild(hs);
			
			goPrint(hs.x, hs.y + 52 * EEConstants.Koef, hs.width, getHight(num));
		}
		
		public function getHight(n: int): int
		{
			var so: SharedObject = SharedObject.getLocal("score");
			if (so.size == 0)
			{
				so.data.hs = 0;
			}
			if (n > so.data.hs) so.data.hs = n;
			return so.data.hs;
		}
		
		private function goPrint(x, y, width, n: int): void
		{
			var data: TextField = new TextField();
			data.embedFonts = true;
			data.width = 100 * EEConstants.Koef;
			data.height = 70 * EEConstants.Koef;
			var format: TextFormat = new TextFormat("trashco", 60 * EEConstants.Koef, 0xFFFFFF);
			data.defaultTextFormat = format;
			data.text = n.toString();
			data.x = x + width;
			data.y = y - 10 * EEConstants.Koef;
			var glow: GlowFilter = new GlowFilter(0x006157, 1, 8 * EEConstants.Koef, 8 * EEConstants.Koef, 40);
            data.filters = [glow];
			this.addChild(data);
		}
		
		public function printScore(n: int): void
		{
			var s: DisplayObject = new Resource.MS();
			s.width *= EEConstants.Koef;
			s.height *= EEConstants.Koef;
			s.y = EEConstants.GeneralHeight * 0.61; //435 * EEConstants.Koef;
			this.addChild(s);
			
			goPrint(s.x, s.y, s.width, n);
		}
	}
	
}