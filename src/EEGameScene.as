package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class EEGameScene extends Sprite 
	{
		public var m: Menu;
		
		private function GeneralGraphic(): void
		{
			var d1: DisplayObject = new Resource.GFon();
			var d2: DisplayObject = new Resource.GFonp();
			d2.width = d1.width *= EEConstants.Koef;
            d2.height = d1.height *= EEConstants.Koef;
			this.addChild(d1);
			this.addChild(d2);
		}
		
		public function EEGameScene()
		{
			GeneralGraphic();
			m = new Menu();
			this.addChild(m);
		}
	}
	
}