package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Koluchka extends Sprite
	{
		private var jih: Jihod;
		private var isHit: Boolean = false;
		
		private function onKMove(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
				e.target.x += EEConstants.DSpeed;
				if ((e.target.hitTestObject(jih) == true) && (this.parent == jih.parent) && (isHit == false)) 
				{
					isHit = true;
					//jih.Dec();
					jih.Die();
				}
			    if (e.target.x > EEConstants.GeneralWidth)
			    {
			    	e.target.removeEventListener(Event.ENTER_FRAME, onKMove);
				    this.parent.removeChild(e.target as Sprite);
			    }
			}	
		}
		
		public function Koluchka(j: Jihod)
		{
			jih = j;
			var d: DisplayObject = new Resource.VKol();
			d.y = 93 * EEConstants.Koef;
			/*
			switch (rand)
			{
				case 0: d.y = 284 * EEConstants.HKoef; break; //284
				case 1: d.y = 369 * EEConstants.HKoef; break; //369
				case 2: d.y = 454 * EEConstants.HKoef; break; //454
				case 3: d.y = 539 * EEConstants.HKoef; break; //539
				case 4: d.y = 624 * EEConstants.HKoef; break; //624
			}*/
			d.width *= EEConstants.Koef;
			d.height *= EEConstants.Koef;
			d.x = -d.width;
			this.addChild(d);
			this.addEventListener(Event.ENTER_FRAME, onKMove);
		}
	}
	
}