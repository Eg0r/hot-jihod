package 
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Soldat extends EESpriteAnim
	{
		private var color: int;
		private var type: int;
		public var way: int;
		private var sh: DisplayObject;
		private var jih: Jihod;
		
		private var h: int = 0;
		
		private var pul: int = 0;
		
		private function onFrame(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
		    	e.target.x += EEConstants.PulSpeed + EEConstants.DSpeed;
		    	if (e.target.x > EEConstants.GeneralWidth)
			    {
			    	e.target.removeEventListener(Event.ENTER_FRAME, onFrame);
			    	(e.target as DisplayObject).parent.removeChild(e.target as DisplayObject);
			    }
				
				for (var i: int = 0; i < (e.target as DisplayObject).parent.numChildren; i++)
				{
					if ((getQualifiedClassName((e.target as DisplayObject).parent.getChildAt(i)) == 'Jihod') 
					     && ((e.target as DisplayObject).parent.getChildAt(i).hitTestObject(e.target as DisplayObject))
						 && (((e.target as DisplayObject).parent.getChildAt(i) as Jihod).way == way))
					{
					   //this.parent.removeChild(this.parent.getChildAt(i)); //delete object
					   ((e.target as DisplayObject).parent.getChildAt(i) as Jihod).Die();
					   e.target.removeEventListener(Event.ENTER_FRAME, onFrame);
			    	   (e.target as DisplayObject).parent.removeChild(e.target as DisplayObject); //delete pula
					}
                }
			}
		}
		
		private function onMove(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
				e.target.x += EEConstants.DSpeed;
				if (type == 1)
				{
					h++;
					if (h == 20) 
					{
						if (way != 5) 
						{ 
							(this.parent.parent as Ways).setChildWay(this, way); 
							way++; 
						}
					}
					if (h == 40)
					{
						if (way != 1) 
						{ 
							(this.parent.parent as Ways).setChildWay(this, way);
							h = 0; 
							way--; 
						}
					}
				}
				if (type == 2)
				{
					if ((jih.way != this.way) && (this.x <= jih.x))
					{
						this.way = jih.way;
						(this.parent.parent as Ways).setChildWay(this, way);
					}
				}
		
			    if (e.target.x > EEConstants.GeneralWidth)
			    {
			    	e.target.removeEventListener(Event.ENTER_FRAME, onMove);
				    this.parent.removeChild(e.target as EESpriteAnim);
			    }
				
				pul++;
				if (pul == EEConstants.PulCount + EEConstants.PulCount * color)
				{
					var p: DisplayObject = new Resource.PSol();
			        p.width *= EEConstants.Koef;
			        p.height *= EEConstants.Koef;
			        p.x = this.x + this.width;
			        p.y = this.y + 91 * EEConstants.Koef;
			        this.parent.addChild(p); 
			        p.addEventListener(Event.ENTER_FRAME, onFrame);
					pul = 0;
				}
			}	
		}
		
		public function Soldat(c: int, t: int, j: Jihod, thisway: int)
		{
			jih = j;
			color = c; 
			type = t;
			sh = new Resource.SSh();
			sh.width *= EEConstants.Koef;
			sh.height *= EEConstants.Koef;
			this.addChild(sh);
			
			var cl: Vector.<Class> = new Vector.<Class>(8);
			
			if (c == 0) 
			{
				cl[0] = Resource.YS1;
				cl[1] = Resource.YS2;
				cl[2] = Resource.YS3;
				cl[3] = Resource.YS4;
				cl[4] = Resource.YS5;
				cl[5] = Resource.YS6;
				cl[6] = Resource.YS7;
				cl[7] = Resource.YS8;
			}
			else
			{
				cl[0] = Resource.GS1;
				cl[1] = Resource.GS2;
				cl[2] = Resource.GS3;
				cl[3] = Resource.GS4;
				cl[4] = Resource.GS5;
				cl[5] = Resource.GS6;
				cl[6] = Resource.GS7;
				cl[7] = Resource.GS8;
			}
			super(cl[0],
				  cl[1],
				  cl[2],
				  cl[3]);
				  
			way = thisway;
				  
			this.x = -this.width;
			this.startAnim();
			this.addEventListener(Event.ENTER_FRAME, onMove);
		}
	}
	
}