package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class InfoAbout extends Sprite
	{
		private var bron: DisplayObject;
		
		public function InfoAbout()
		{
			bron = new Resource.IBron;
			bron.width *= EEConstants.Koef;
			bron.height *= EEConstants.Koef;
			bron.y = bron.height;
			this.addChild(bron);
		}
		
		public function setVisio(): void
		{
			bron.visible = true;
		}
		
		public function unSetVisio(): void
		{
			bron.visible = false;
		}
	}
	
}