package 
{
	//import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class EEDisplayObject extends DisplayObject 
	{
		public var d: DisplayObject;
		
		private function onAdd(e: Event): void
		{
			d.width *= EEConstants.Koef;
			d.height *= EEConstants.Koef;
			
		}
		
		public function EEDisplayObject(a: Class)
		{
			super();
	        d = new a();
			this.addEventListener(Event.ADDED_TO_STAGE, onAdd);
		}
	}
	
}