package 
{
		import flash.display.Sprite;
		import flash.events.Event;
		import flash.text.TextField;
		import flash.text.TextFieldAutoSize;
		import flash.utils.getTimer;
        
        public class EEFPSMeter extends Sprite//класс для измерения фпс. может быть добавлен и расположен в любой тчке экрана
        {
                private var lastFrameTime:Number;
                private var output:TextField;
                
                public function EEFPSMeter(posX:Number=0,posY:Number=0)
                {
                        output=new TextField();//текстовое поле. будет расположено на экране
                        output.autoSize=TextFieldAutoSize.LEFT;//автоформат по левому краю
                        output.selectable=false;//поле не реагирует на мышь.следовательно не может помешать игре
                        output.x=posX;
                        output.y=posY;
                        addChild(output);
                        addEventListener(Event.ENTER_FRAME,enterFrameListener);
                }
                private function enterFrameListener(e:Event):void{
                        var now:Number=getTimer();
                        var elapsed:Number=now-lastFrameTime;
                        var fps:Number=Math.round(1000/elapsed); 
                        output.text = "Extrapolated actual fps:" + fps + "\nDesignated fps:" + stage.frameRate+"\nStage Height" + stage.stageHeight + "\nStage Width" + stage.stageWidth +
						"\nStage Height2 " + EEConstants.GeneralHeight + "\nStage Width2 " + EEConstants.GeneralWidth;
                        lastFrameTime=now;
                }
        }
	
}