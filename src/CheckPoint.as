package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class CheckPoint extends Sprite 
	{
		private var yar: DisplayObject;
		private var bigs: DisplayObject;
		private var data: TextField;
		private var data2: TextField;
		
		private var count: int = 0;
		private var displaynum: int = 0;
		
		public function CheckPoint()
		{
			yar = new Resource.CYar();
			yar.width *= EEConstants.Koef;
			yar.height *= EEConstants.Koef;
			this.addChild(yar);
			
			data = new TextField();
			data.embedFonts = true;
			data.width = 130 * EEConstants.Koef;
			data.height = 60 * EEConstants.Koef;
			var format: TextFormat = new TextFormat("trashco", 60 * EEConstants.Koef, 0xFFFFFF);
			data.defaultTextFormat = format;
			data.text = count.toString();
			data.x = yar.width;
			this.addChild(data);
			
			bigs = new Resource.CCh();
			bigs.width *= EEConstants.Koef;
			bigs.height *= EEConstants.Koef;
			bigs.x = EEConstants.GeneralWidth / 2 - bigs.width / 2;
			bigs.y = EEConstants.GeneralHeight / 2 - bigs.height / 2;
			bigs.visible = false;
			this.addChild(bigs);
			
			data2 = new TextField();
			data2.embedFonts = true;
			data2.width = 220 * EEConstants.Koef;
			data2.height = 220 * EEConstants.Koef;
			var format2: TextFormat = new TextFormat("trashco", 200 * EEConstants.Koef, 0x000000);
			format2.align = TextFormatAlign.CENTER;
			format2.indent = 0;
			format2.blockIndent = 0;
			data2.defaultTextFormat = format2;
			data2.text = count.toString();
			data2.x = EEConstants.GeneralWidth / 2 - data2.width / 2;
			data2.y = EEConstants.GeneralHeight / 2 - data2.height / 2 - 200 * EEConstants.Koef / 4;
			data2.visible = false;
			this.addChild(data2);
		}
		
		private function visiableCount(e: Event): void
		{
			displaynum++;
			if (displaynum > 15)
			{
				bigs.visible = false;
			    data2.visible = false;
				displaynum = 0;
				e.target.removeEventListener(Event.ENTER_FRAME, visiableCount);
			}
		}
		
		public function Inc(): void
		{
			count++;
			data.text = count.toString();
			data2.text = count.toString();
			bigs.visible = true;
			data2.visible = true;
			this.addEventListener(Event.ENTER_FRAME, visiableCount);
		}
		
		public function getScore(): int 
		{
			return count;
		}
	}
	
}