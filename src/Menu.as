package 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.desktop.NativeApplication;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Menu extends Sprite
	{
		private var d: EESpriteAnim;
		private var g: Game;
		private var sc: Score;
		public var pl: EESoundPlayer;
		public var butSound: EEButton;
		
		private function GoGame(e: Event): void
		{
			this.visible = false;
			g = new Game();
			this.parent.addChild(g);
			this.parent.addChild(butSound);
		}
		
		public function retMenu(n: int): void
		{
			this.visible = true;
			this.parent.removeChild(g);
			g = null;
			this.removeChild(sc);
			sc = null;
			sc = new Score(n);
			sc.printScore(n);
			this.addChild(sc);
		}
		
		private function onActive(e: Event): void
		{
			if (butSound.onoroff == false)
			{
				pl.PlayLoop();
			}
		}
		
		private function onDeactive(e: Event): void
		{
			this.pl.Pause();
		}
		
		public function Menu()
		{
			var d1: DisplayObject = new Resource.Mpodl();
			d1.width *= EEConstants.Koef;
            d1.height = EEConstants.GeneralHeight * 0.61;// *= EEConstants.Koef;
			this.addChild(d1);
					
			d = new EESpriteAnim(Resource.MName,
								Resource.MName1,
								Resource.MName2,
								Resource.MName3,
								Resource.MName4,
								Resource.MName5,
								Resource.MName6);
			d.height = EEConstants.GeneralHeight * 0.61; 
			this.addChild(d);
			d.startAnim();
			
			var but: EEButton = new EEButton(Resource.MStart);
			but.x = 450 * EEConstants.Koef;
			but.y = EEConstants.GeneralHeight * 0.61; //435 * EEConstants.Koef;
			this.addChild(but);
			but.AddonClick(GoGame);
			
			pl = new EESoundPlayer(Resource.Music);
			pl.PlayLoop();
			
			butSound = new EEButton(Resource.MSound);
			butSound.x = EEConstants.GeneralWidth - butSound.width;
			butSound.y = 1.5*butSound.height;
			butSound.AddPictureState(Resource.MSoundOff);
			this.addChild(butSound);
			butSound.AddonClick(function(e: Event): void { if (butSound.onoroff == true) pl.Pause() else pl.PlayLoop(); } );
			
			sc = new Score(0); sc.printScore(0);
			this.addChild(sc);
			
			NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, onActive);
			NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, onDeactive);
		}
	}
	
}