package 
{
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class SplashScreen2 extends Sprite
	{
		[Embed(source = "Graphics/logo.png")] public static var SImage: Class;
		
		private var time: int;
		
		public function SplashScreen2()
		{
			this.cacheAsBitmap = true;
			
			var shape: Shape = new Shape();
			var gr: Graphics = shape.graphics;
			gr.beginFill(0x000000);
			gr.drawRect(0, 0, EEConstants.GeneralWidth, EEConstants.GeneralHeight);
			gr.endFill();
			
			var image: DisplayObject = new SImage();
			image.width *= EEConstants.HKoef;
			image.height *= EEConstants.HKoef;
			image.x = EEConstants.GeneralWidth / 2 - image.width / 2;
			
			this.addChild(shape);
			this.addChild(image);
			
			time = 60;
			this.addEventListener(Event.ENTER_FRAME, onTime);
		}
		
		private function onTime(e: Event): void
		{
			time--;
			if (time < 0)
			{
				this.removeEventListener(Event.ENTER_FRAME, onTime);
				this.parent.removeChild(this);
			}
		}
	}
	
}