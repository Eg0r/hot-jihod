package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class EESpriteAnim extends Sprite 
	{
		private var image: Array;
		private var tick: int;
		private var now: int;
		
		private function visiable(x: int): void
		{
			for (var i: int = 0; i < image.length; i++)
			{
				if (i != x) image[i].visible = false;
			}
			image[x].visible = true;
		}
		
		private function onFrame(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
			   tick++;
			   if (now * EEConstants.ASpeed + EEConstants.ASpeed == tick) 
			   {
				   now++;
				   if ((image.length - 1) * EEConstants.ASpeed + EEConstants.ASpeed == tick) { tick = 0; now = 0; }
				   visiable(now);
			   }
			}
		}
		
		public function startAnim(): void
		{
			tick = 0;
			now = 0;
			this.addEventListener(Event.ENTER_FRAME, onFrame);
		}
		
		public function setXY(x: int, y: int): void
		{
			this.x = x;
			this.y = y;
		}
		
		public function EESpriteAnim(...rest)
		{
			image = new Array(rest.length);
			for (var i: int = 0; i < rest.length; i++)
			{
				image[i] = new rest[i]();
				image[i].width *= EEConstants.Koef;
				image[i].height *= EEConstants.Koef;
				image[i].cacheAsBitmap = true;
				if (i != 0) image[i].visible = false;
				this.addChild(image[i]);
			}
		}
	}
	
}