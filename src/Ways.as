package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Ways extends Sprite 
	{
		private static var WAYS_POSITION: Array = new Array(150, 
															235, 
															320, 
															405, 
															490);
		
		private var waysSprite: Vector.<Sprite>; 
		
		public function setChildWay(child: Sprite, way: int): void
		{
			if (child.parent != null) child.parent.removeChild(child);
			waysSprite[way - 1].addChild(child);
		}
		
		public function Ways()
		{
			waysSprite = new Vector.<Sprite>(5);
			
			this.y = WAYS_POSITION[0] * EEConstants.HKoef;
			
			for (var i: int = 0; i < 5; i++)
			{
				waysSprite[i] = new Sprite();
				waysSprite[i].y = (WAYS_POSITION[i] - WAYS_POSITION[0]) * EEConstants.HKoef;
				this.addChild(waysSprite[i]);
			}
		}
	}
	
}