package 
{
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Resource 
	{
		//Общее 
		[Embed(source = "Graphics/Фон/фон.png")] public static var GFon: Class;
		[Embed(source = "Graphics/Фон/фон верхняя подложка.png")] public static var GFonp: Class;
		
		//Меню 
		[Embed(source = "Graphics/меню/HotJihot подложка.png")] public static var Mpodl: Class;
		[Embed(source = "Graphics/меню/HotJihot1.png")] public static var MName1: Class;
		[Embed(source = "Graphics/меню/HotJihot2.png")] public static var MName2: Class;
		[Embed(source = "Graphics/меню/HotJihot3.png")] public static var MName3: Class;
		[Embed(source = "Graphics/меню/HotJihot4.png")] public static var MName4: Class;
		[Embed(source = "Graphics/меню/HotJihot5.png")] public static var MName5: Class;
		[Embed(source = "Graphics/меню/HotJihot6.png")] public static var MName6: Class;
		[Embed(source = "Graphics/меню/HotJihot.png")] public static var MName: Class;
	    [Embed(source = "Graphics/меню/Играть.png")] public static var MStart: Class;
		[Embed(source = "Graphics/меню/звук.png")] public static var MSound: Class;
		[Embed(source = "Graphics/меню/звук выкл.png")] public static var MSoundOff: Class;
		[Embed(source = "Graphics/меню/пауза.png")] public static var MPause: Class;
		[Embed(source = "Graphics/меню/highscore.png")] public static var MHS: Class;
		[Embed(source = "Graphics/меню/score.png")] public static var MS: Class;
		
		//Фон 
		[Embed(source = "Graphics/Фон/фон анимация1.png")] public static var FAnim1: Class;
		[Embed(source = "Graphics/Фон/фон анимация2.png")] public static var FAnim2: Class;
		[Embed(source = "Graphics/Фон/фон анимация3.png")] public static var FAnim3: Class;
		[Embed(source = "Graphics/Фон/фон анимация4.png")] public static var FAnim4: Class;
		[Embed(source = "Graphics/Фон/фон анимация5.png")] public static var FAnim5: Class;
		[Embed(source = "Graphics/Фон/фон анимация6.png")] public static var FAnim6: Class;
		[Embed(source = "Graphics/Фон/фон анимация7.png")] public static var FAnim7: Class;
		[Embed(source = "Graphics/Фон/фон анимация8.png")] public static var FAnim8: Class;
		[Embed(source = "Graphics/Фон/фон анимация9.png")] public static var FAnim9: Class;
		[Embed(source = "Graphics/Фон/верблюд.png")] public static var FVerb: Class;
		[Embed(source = "Graphics/Фон/дерево1.png")] public static var FTree1: Class;
		[Embed(source = "Graphics/Фон/дерево2.png")] public static var FTree2: Class;
		[Embed(source = "Graphics/Фон/облако1.png")] public static var FSkyO1: Class;
		[Embed(source = "Graphics/Фон/облако2.png")] public static var FSkyO2: Class;
		[Embed(source = "Graphics/игра/Чекпоинт вышка1.png")] public static var FTop1: Class;
		[Embed(source = "Graphics/игра/Чекпоинт вышка2.png")] public static var FTop2: Class;
		
		//Джиход 
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист1.png")] public static var Jihod1: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист2.png")] public static var Jihod2: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист3.png")] public static var Jihod3: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист4.png")] public static var Jihod4: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист5.png")] public static var Jihod5: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист6.png")] public static var Jihod6: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист7.png")] public static var Jihod7: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист8.png")] public static var Jihod8: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист9.png")] public static var Jihod9: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист10.png")] public static var Jihod10: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист11.png")] public static var Jihod11: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист12.png")] public static var Jihod12: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист13.png")] public static var Jihod13: Class;
		[Embed(source = "Graphics/игра/персонажи/Террорист/Террорист тень.png")] public static var JihodSh: Class;
		
		//Солдаты
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый1.png")] public static var YS1: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый2.png")] public static var YS2: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый3.png")] public static var YS3: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый4.png")] public static var YS4: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый5.png")] public static var YS5: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый6.png")] public static var YS6: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый7.png")] public static var YS7: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Жёлтый/Жёлтый8.png")] public static var YS8: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный1.png")] public static var GS1: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный2.png")] public static var GS2: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный3.png")] public static var GS3: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный4.png")] public static var GS4: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный5.png")] public static var GS5: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный6.png")] public static var GS6: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный7.png")] public static var GS7: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный8.png")] public static var GS8: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный9.png")] public static var GS9: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный10.png")] public static var GS10: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный11.png")] public static var GS11: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный12.png")] public static var GS12: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/Зелёный/Зелёный13.png")] public static var GS13: Class;
		[Embed(source = "Graphics/игра/персонажи/Солдаты/тень.png")] public static var SSh: Class;
		
		//Управление
		[Embed(source = "Graphics/игра/кнопка верх.png")] public static var UUpBut: Class;
		[Embed(source = "Graphics/игра/кнопка низ.png")] public static var UDownBut: Class;
		[Embed(source = "Graphics/игра/огонь.png")] public static var UFire: Class;
		[Embed(source = "Graphics/игра/огонь нажат.png")] public static var UFireCl: Class;
		
		//Пули
		[Embed(source = "Graphics/игра/персонажи/пуля террорист.png")] public static var PTer: Class;
		[Embed(source = "Graphics/игра/персонажи/пуля солдаты.png")] public static var PSol: Class;
		
		//Чекпоинты
		[Embed(source = "Graphics/игра/ярлык чекпоинт.png")] public static var CYar: Class;
		[Embed(source = "Graphics/игра/Чекпоинт.png")] public static var CCh: Class; 
		
		//Взаимодействие
		[Embed(source = "Graphics/игра/объекты/колючка.png")] public static var VKol: Class;
		[Embed(source = "Graphics/игра/объекты/ящик огонь очередью.png")] public static var VYaOchF: Class;
		[Embed(source = "Graphics/игра/объекты/ящик броня.png")] public static var VYaBr: Class;
		
		[Embed(source = "Graphics/игра/ярлык бронижелет.png")] public static var IBron: Class;
		
		
		//Звуки
		[Embed(source = "Sound/mojahed.mp3")] public static var Music: Class;
		
		[Embed(source = "font.ttf", fontName = "trashco", mimeType="application/x-font", advancedAntiAliasing="true",
        embedAsCFF="false")] public static var trashco: Class;
	}
	
}