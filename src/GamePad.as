package 
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class GamePad extends Sprite 
	{
		private var DB: EEButton;
		private var UB: EEButton;
		private var FB: EEButton;
		
		public function GamePad()
		{
			DB = new EEButton(Resource.UDownBut);
			UB = new EEButton(Resource.UUpBut);
			
			FB = new EEButton(Resource.UFire);
			FB.AddPicture(Resource.UFireCl);
			
			UB.x = DB.x = EEConstants.GeneralWidth - DB.width;
			DB.y = EEConstants.GeneralHeight - DB.height;
			UB.y = EEConstants.GeneralHeight - UB.height - UB.height;
			FB.y = EEConstants.GeneralHeight - FB.height;
			
			
			this.addChild(DB);
			this.addChild(UB);
			this.addChild(FB);
		}
		
		public function addUP(f: Function): void
		{
			UB.AddonClick(f);
		}
		
		public function addDOWN(f: Function): void
		{
			DB.AddonClick(f);
		}
		
		public function addFIRE(f: Function, f2: Function): void
		{
			FB.AddMouseDown(f); 
			FB.AddMouseUp(f2);
		}
	}
	
}