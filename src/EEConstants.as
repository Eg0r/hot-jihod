package 
{
	import flash.system.Capabilities;
	import flash.display.DisplayObject;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class EEConstants extends DisplayObject
	{
		public static function init(height: Number): void
		{
			GeneralHeight = height;
			HKoef = GeneralHeight / 768;
		}
		
		public static var Koef: Number = Capabilities.screenResolutionX / 1024;
		public static var HKoef: Number;
		public static var GeneralWidth: Number = Capabilities.screenResolutionX;
		public static var GeneralHeight: Number;// = Capabilities.screenResolutionY;
		public static var ASpeed: int = 4;
		public static var isPause: Boolean = false;
		public static var fps: int = 30;
		
		public static var DSpeed: int = 5;
		public static var PulSpeed: int = 8;
		public static var PulCount: int = 16;
	}
	
}