package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class BackFon extends Sprite
	{
		private var r: int = 3;
		private var h: int = 260 * EEConstants.Koef / EEConstants.DSpeed;
		private var hnow: int = 0;
		
		private var now: int = 0;
		private var line: int = 400;
		private var top: EESpriteAnim;
		private var showtop: Boolean = false;
		private var ispoint: Boolean = false;
		private var point: CheckPoint;
		
		private var isHit: Boolean = false;
		
		private var ways: Ways;
		
		public function getPoint(): CheckPoint
		{
			return point;
		}
		
		private function onTopMove(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
			    if (showtop == true)
			    {
			     	top.x += EEConstants.DSpeed;
			    	if (top.x > EEConstants.GeneralWidth)
			    	{
			    		showtop = false;
			    	}
			    	if ((top.x > 650 * EEConstants.Koef)&&(ispoint==false))
				    {
				    	ispoint = true;
			 	    	point.Inc();
			    	}
			    }
			}
		}
		
		private function visibleTop(): void
		{
			showtop = true;
			ispoint = false;
			if (top == null)
			{
				top = new EESpriteAnim(Resource.FTop1, Resource.FTop2);
				top.x = - top.width;
				this.addChild(top);
				this.addEventListener(Event.ENTER_FRAME, onTopMove);
				top.startAnim();
			}
			else
			{
				top.x = - top.width;
			}
			
			(this.parent as Game).addLevel();
		}
		
		private function CreateObj(): void
		{
			var rand: int = Math.round(Math.random() * 4);
			var newobj: DisplayObject;
			switch (rand)
			{
				case 0: newobj = new Resource.FVerb(); break;
				case 1: newobj = new Resource.FTree1(); break;
				case 2: newobj = new Resource.FTree2(); break;
				case 3: newobj = new Resource.FSkyO1(); break;
				case 4: newobj = new Resource.FSkyO2(); break;
			}
			newobj.width *= EEConstants.Koef;
			newobj.height *= EEConstants.Koef;
			newobj.x = -newobj.height;
			this.addChild(newobj);
			newobj.addEventListener(Event.ENTER_FRAME, onMove);
		}
		
		private function onFrame(e: Event): void 
		{
			if (EEConstants.isPause == false)
			{
			    if (hnow < h) hnow++;
				now++;
				if (now % line == 0)
				{
					visibleTop();
					hnow = 0;
				}
				else 
				{
					if (hnow == h)
					{
						var rand: int = Math.round(Math.random() * r);
						if (rand == 0)
						{
							CreateObj();
						}
						hnow = 0;
					}
				}
			}
		}
		
		private function onMove(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
				e.target.x += EEConstants.DSpeed;
			    if (e.target.x > EEConstants.GeneralWidth)
			    {
			    	e.target.removeEventListener(Event.ENTER_FRAME, onMove);
				    this.removeChild(e.target as DisplayObject);
			    }
			}	
		}
		
		public function addPoint(p: CheckPoint): void
		{
			point = p;
		}
		
		public function BackFon()
		{		
			this.addEventListener(Event.ENTER_FRAME, onFrame);
		}
	}
	
}