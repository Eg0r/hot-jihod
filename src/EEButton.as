package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class EEButton extends Sprite
	{
		private var d: DisplayObject;
		private var d2: DisplayObject;
		public var onoroff: Boolean = false;
		
		public function EEButton(a: Class)
		{
			d = new a();
			d.width *= EEConstants.Koef;
			d.height *= EEConstants.Koef;
			this.addChild(d);
		}
		
		private function MouseDown(e: MouseEvent): void
		{
			if (EEConstants.isPause == false)
			{
			   d2.visible = true;
			   d.visible = false;
			}
		}
		
		private function MouseDown2(e: MouseEvent): void
		{
			if (EEConstants.isPause == false)
			{
			   onoroff = !onoroff && true || onoroff && false;
			   if (onoroff == true)
			   {
				   d2.visible = true;
			       d.visible = false;
			   }
			   else
			   {
				   d2.visible = false;
			       d.visible = true;
			   }
			}
		}
		
		private function MouseUp(e: MouseEvent): void
		{
			d2.visible = false;
			d.visible = true;
		}
		
		public function AddonClick(f: Function): void
		{
			this.addEventListener(MouseEvent.CLICK, f);
		}
		
		public function AddMouseDown(f: Function): void
		{
			this.addEventListener(MouseEvent.MOUSE_DOWN, f);
		}
		
		public function AddMouseUp(f: Function): void
		{
			this.addEventListener(MouseEvent.MOUSE_UP, f);
		}
		
		public function AddPicture(b: Class): void 
		{
			d2 = new b();
			d2.width *= EEConstants.Koef;
			d2.height *= EEConstants.Koef;
			d2.visible = false;
			this.addChild(d2);
			this.addEventListener(MouseEvent.MOUSE_DOWN, MouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, MouseUp);
		}
		
		public function AddPictureState(b: Class): void
		{
			d2 = new b();
			d2.width *= EEConstants.Koef;
			d2.height *= EEConstants.Koef;
			d2.visible = false;
			this.addChild(d2);
			this.addEventListener(MouseEvent.MOUSE_DOWN, MouseDown2);
		}
	}
	
}