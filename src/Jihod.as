package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Jihod extends EESpriteAnim
	{
		private var sh: DisplayObject;
		
		public var way: int;
		
		private var fireON: Boolean = false;
		private var pul: int = 0;
		
		private var lives: int = 3;
		
		private var isHit: Boolean = false;
		private var hitcount: int = 0;
		
		private var diepart: Boolean = false;
		
		private var info: InfoAbout;
		
		private var countSHIT: int = 0;
		private var timeFIRE: int = 0;
	//	private var flagFIRE: Boolean = false;
		
		public function Jihod(infa: InfoAbout)
		{
			sh = new Resource.JihodSh();
			sh.width *= EEConstants.Koef;
			sh.height *= EEConstants.Koef;
			this.addChild(sh);
			super(Resource.Jihod1,
			      Resource.Jihod2,
				  Resource.Jihod3,
				  Resource.Jihod4,
				  Resource.Jihod5,
				  Resource.Jihod6,
				  Resource.Jihod7,
				  Resource.Jihod8,
				  Resource.Jihod9,
				  Resource.Jihod10,
				  Resource.Jihod11,
				  Resource.Jihod12,
				  Resource.Jihod13);
			this.setXY(650 * EEConstants.Koef, 0);
			way = 1;
			this.startAnim();
			
			this.addEventListener(Event.ENTER_FRAME, onFire);
			
			
			info = infa;
		}
		
		private function onDec(e: Event): void
		{
			this.visible = !this.visible;
			hitcount++;
			if (hitcount == 50)
			{
				isHit = false;
				hitcount = 0;
				e.target.removeEventListener(Event.ENTER_FRAME, onDec);
			}
		}
		
		public function Dec(): void
		{
			if (isHit == false)
			{
			   isHit = true;
			   lives--;
			   if (lives == 0) Die() else this.addEventListener(Event.ENTER_FRAME, onDec);
			}
		}
		
		public function Up(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
			   if (way != 1)
			   {
				   way--;
				   (this.parent.parent as Ways).setChildWay(this, way);
			   }
			}
		}
		
		public function Down(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
			   if (way != 5)
			   {
				   way++;
				   (this.parent.parent as Ways).setChildWay(this, way);
			   }
			}
		}
		
		private function onFrame(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
		    	e.target.x -= EEConstants.PulSpeed;
				for (var i: int = 0; i < (e.target as DisplayObject).parent.numChildren; i++)
				{
					if ((getQualifiedClassName((e.target as DisplayObject).parent.getChildAt(i)) == 'Soldat') 
					     && ((e.target as DisplayObject).parent.getChildAt(i).hitTestObject(e.target as DisplayObject)))
					{
					   (e.target as DisplayObject).parent.removeChild((e.target as DisplayObject).parent.getChildAt(i));
					   e.target.removeEventListener(Event.ENTER_FRAME, onFrame);
			    	   (e.target as DisplayObject).parent.removeChild(e.target as DisplayObject);
					}
                }
				
		    	if (e.target.x < 5)
			    {
			    	e.target.removeEventListener(Event.ENTER_FRAME, onFrame);
			    	(e.target as DisplayObject).parent.removeChild(e.target as DisplayObject);
			    }
			}
		}
		
		private function onFire(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
				if (timeFIRE > 0) timeFIRE--;
			    if (fireON == true)
			    {
			    	if (pul == 0) Fire();
			    	pul++;
			    	if (timeFIRE > 0) 
					{ 
						if (pul >= EEConstants.PulCount / 2) pul = 0;
					} 
					else 
					{ 
						if (pul >= EEConstants.PulCount) pul = 0;
					}
			    }
			    if (fireON == false)
			    {
				    if (pul != 0)
				    {
						pul++;
						if (timeFIRE > 0) 
						{
							if (pul >= EEConstants.PulCount / 2) pul = 0;
						} 
						else 
						{
							if (pul >= EEConstants.PulCount) pul = 0;
						}
				    }
			    }
			}
		}
		
		private function Fire(): void 
		{ 
			   var p: DisplayObject = new Resource.PTer; 
			   p.x = this.x;
			   p.y = this.y + 91 * EEConstants.Koef;
			   p.width *= EEConstants.Koef;
			   p.height *= EEConstants.Koef;
			   this.parent.addChild(p); 
			   p.addEventListener(Event.ENTER_FRAME, onFrame);
		}
		
		public function FireBegin(e: Event): void
		{
			fireON = true;
		}
		
		public function FireEnd(e: Event): void
		{
			fireON = false;
		}
		
		public function Ochered(): void
		{
			timeFIRE = 250;
		}
		
		public function Save(): void
		{
			//timeSHIT = 250;
			countSHIT = 1;
			info.setVisio();
		}
		
		private function onDie(e: Event): void
		{
			//if (this.y > 15 * EEConstants.Koef)
			{
				this.visible = !this.visible;
				
				if (diepart == false) 
				{
					this.y -= 15 * EEConstants.Koef
					if (this.y <= -15 * EEConstants.Koef) diepart = true;
					
					//this.parent.removeChild(this);
				}
				if (diepart == true) 
				{
					this.y += 15 * EEConstants.Koef;	
					if (this.y > EEConstants.GeneralHeight)
					{
						this.removeEventListener(Event.ENTER_FRAME, onDie);
						(this.parent.parent.parent.parent.getChildAt(2) as Menu).retMenu((this.parent.parent.parent as Game).getBF().getPoint().getScore());
					}
				}
			}
		}
		
		public function Die(): void
		{
			if (countSHIT == 0)
			{
				this.addEventListener(Event.ENTER_FRAME, onDie);
			} 
			else
			{
				countSHIT = 0;
				info.unSetVisio();
			}
		}
	}
	
}