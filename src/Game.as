package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.desktop.NativeApplication;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Game extends Sprite
	{
		private var duns: EESpriteAnim;
		
		private var butPause: EEButton;
		private var butPlay: EEButton;
		
		private var bf: BackFon;
		
		private var ways: Ways;
		
		private var point: CheckPoint;
		
		private var jih: Jihod;
		
		private var inf: InfoAbout;
		
		private var h: int = 160 * EEConstants.Koef / EEConstants.DSpeed;
		private var hnow: int = 0;
		
		private var lvla: LvlAlgorytm;
		
		private static var step: int = (EEConstants.GeneralHeight - EEConstants.GeneralHeight * 0.36) / 5;
		
		private function onActive(e: Event): void
		{
            stage.frameRate = EEConstants.fps;
		}
		
		private function onDeactive(e: Event): void
		{
			stage.frameRate = 4;
			this.Pause(e);
		}
		
		private function Pause(e: Event): void
		{
			EEConstants.isPause = true;
			butPlay.visible = true;
			((this.parent as EEGameScene).m as Menu).pl.Pause();
		}
		
		private function Play(e: Event): void
		{
			EEConstants.isPause = false;
			butPlay.visible = false;
			if (((this.parent as EEGameScene).m as Menu).butSound.onoroff == false)
			{
				((this.parent as EEGameScene).m as Menu).pl.PlayLoop();
			}
		}
		
		public function getBF(): BackFon
		{
			return bf;
		}
		
		private function onFrame(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
				if (hnow < h) hnow++;
				if (hnow == h)
				{
					var rand: int = Math.round(Math.random() * 2);
					if (rand == 0)
					{
						if (lvla.calcKoluchkaget())
						{
							rand = Math.round(Math.random() * 4);
							ways.setChildWay(new Koluchka(jih), rand + 1);
						}
					}
					if (rand == 1)
					{
						if (lvla.calcBoxget())
						{
							rand = Math.round(Math.random() * 4);
							ways.setChildWay(new Box(Math.round(Math.random()), jih), rand + 1);
						}
					}
					if (rand == 2)
					{
						var v: int = lvla.calcSoldatget();
						if (v>=0)
						{
							rand = Math.round(Math.random());
							var thisway: int = Math.round(Math.random() * 4) + 1;
							ways.setChildWay(new Soldat(rand, v/*Math.round(Math.random() * 2)*/, jih, thisway), thisway);
						}
					}
					hnow = 0;
				}
			}
		}
		
		public function addLevel(): void
		{
			lvla.addTop();
		}
		
		public function Game()
		{
			//Дюны пустыни
			duns = new EESpriteAnim(Resource.FAnim1, 
			                        Resource.FAnim2,
									Resource.FAnim3,
									Resource.FAnim4,
									Resource.FAnim5,
									Resource.FAnim6,
									Resource.FAnim7,
									Resource.FAnim8,
									Resource.FAnim9);
			this.addChild(duns);
			duns.startAnim();
			
			//Задний фон (верблюды, облака, деревья, вышки)
			bf = new BackFon();			
			this.addChild(bf);
			
			//Дорожки 
			ways = new Ways();
			this.addChild(ways);
			
			//Всплывающая инфа о вышке
			point = new CheckPoint();
			bf.addPoint(point);
			this.addChild(point);
			
			inf =  new InfoAbout();
			inf.unSetVisio();
			this.addChild(inf);
			
			//Джиход
			jih = new Jihod(inf);
			ways.setChildWay(jih, 1);
			
			//Управление 
			var gp: GamePad = new GamePad();
			this.addChild(gp);
			
			//Инициализация управления
			gp.addUP(jih.Up);
			gp.addDOWN(jih.Down);
			gp.addFIRE(jih.FireBegin, jih.FireEnd);
			
			//Кнопка пауза
			butPause = new EEButton(Resource.MPause);
			butPause.x = EEConstants.GeneralWidth - butPause.width;
			butPause.AddonClick(Pause);
			this.addChild(butPause);
			
			//Кнопка старт
			butPlay = new EEButton(Resource.MStart);
			butPlay.x = 450 * EEConstants.Koef;
			butPlay.y = EEConstants.GeneralHeight / 2 - butPlay.height / 2;
			this.addChild(butPlay);
			butPlay.AddonClick(Play);
			butPlay.visible = false;
			
			//Алгоритм усложнения
			lvla = new LvlAlgorytm();
			
			this.addEventListener(Event.ENTER_FRAME, onFrame);
			
			NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, onActive);
			NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, onDeactive);
		}
	}
	
}