package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.DisplayObject;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Box extends Sprite 
	{
		private var jih: Jihod;
		private var type: int;
		private var isHit: Boolean = false;
		
		private function onMove(e: Event): void
		{
			if (EEConstants.isPause == false)
			{
				e.target.x += EEConstants.DSpeed;
				if ((e.target.hitTestObject(jih) == true) && (this.parent == jih.parent) && (isHit==false)) 
				{
					isHit = true;
					e.target.removeEventListener(Event.ENTER_FRAME, onMove);
				    this.parent.removeChild(e.target as Sprite);
					if (type == 0)
					{
						jih.Ochered();
					}
					if (type == 1)
					{
						jih.Save();
					}
				}
			    if (e.target.x > EEConstants.GeneralWidth)
			    {
			    	e.target.removeEventListener(Event.ENTER_FRAME, onMove);
				    this.parent.removeChild(e.target as Sprite);
			    }
			}	
		}
		
		public function Box(r: int, j: Jihod)
		{
			jih = j;
			var d: DisplayObject;
			switch (r)
			{
				case 0: d = new Resource.VYaOchF(); type = 0; break;
				case 1: d = new Resource.VYaBr(); type = 1; break;
			}
			d.y = 67 * EEConstants.Koef;
			/*
			switch (rand)
			{
				case 0: d.y = 254 * EEConstants.HKoef; break;
				case 1: d.y = 339 * EEConstants.HKoef; break;
				case 2: d.y = 424 * EEConstants.HKoef; break;
				case 3: d.y = 509 * EEConstants.HKoef; break;
				case 4: d.y = 594 * EEConstants.HKoef; break;
			}*/
			d.width *= EEConstants.Koef;
			d.height *= EEConstants.Koef;
			d.x = -d.width;
			this.addChild(d);
			this.addEventListener(Event.ENTER_FRAME, onMove);
		}
	}
	
}