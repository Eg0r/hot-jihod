package 
{
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	
	/**
	 * ...
	 * @author Egor
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, deactivate);
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			// entry point
			
			EEConstants.init(stage.stageHeight);
			
		//	addChild(new EEGameScene());
			//addChild(new EEFPSMeter());
			
			// new to AIR? please read *carefully* the readme.txt files!
		}
		
		private function init(e: Event): void
		{
			var splash: SplashScreen2 = new SplashScreen2();
			this.addChild(splash);
			splash.addEventListener(Event.REMOVED, onRemove);
			this.removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function deactivate(e:Event):void 
		{
			// make sure the app behaves well (or exits) when in background
			//NativeApplication.nativeApplication.exit();
		}
		
		private function onRemove(e: Event): void
		{
			(e.target as SplashScreen2).removeEventListener(Event.REMOVED, onRemove);
			addChild(new EEGameScene());
		}
		
	}
	
}