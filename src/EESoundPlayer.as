package 
{
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	
	/**
	 * ...
	 * @author Egor
	 */
	public class EESoundPlayer 
	{
		private var snd: Sound;
		private var ch: SoundChannel;
		private var pausePosition: int = 0;
		
		private var isplay: Boolean = false;
		
		public function EESoundPlayer(a: Class)
		{
			snd = new a(); 
		}
		
		public function PlayLoop(): void
		{
			if (isplay == false)
			{
				ch = snd.play(pausePosition, 1000);	
				isplay = true;
			}
		}
		
		public function VolumeOff(): void
		{
			SoundMixer.soundTransform = new SoundTransform(0, 0);
		}
		
		public function Pause(): void
		{
			if (isplay == true)
			{
				pausePosition = ch.position; 
				ch.stop();
				isplay = false;
			}
		}
		
		public function VolumeOn(): void
		{
			SoundMixer.soundTransform = new SoundTransform(1, 0);
		}
	}
	
}